#include <iostream>
#include <algorithm>
#include <string>
#include <clocale>
#include <fstream>
#include <vector>
#include <sstream>
#include <random>
#include <chrono>
#include "point.h"
#include "centroid.h"
#include "simple_svg_1.0.0.hpp"

using namespace svg;
using namespace std;

bool centroidsReachedEquilibrium(vector<centroid> &centroids);

void clearPointsFromCentroids(vector<centroid> &centroidList);

void assignPointsToCentroids(vector<point> points, vector<centroid> &centroids);

centroid *findNearestCentroid(point point, vector<centroid> &centroid);

void centerAllCentroids(vector<centroid> &centroids);

void drawSvg(vector<centroid> &centroids, float max);

template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}


vector<string> splitString(string str, string splitBy) {
    vector<string> tokens;
    tokens.push_back(str);
    size_t splitAt;
    size_t splitLen = splitBy.size();
    std::string frag;
    while (true) {
        frag = tokens.back();
        splitAt = frag.find(splitBy);
        if (splitAt == string::npos) {
            break;
        }
        tokens.back() = frag.substr(0, splitAt);
        tokens.push_back(frag.substr(splitAt + splitLen, frag.size() - (splitAt + splitLen)));
    }
    return tokens;
}

float calculate_distance(point p, centroid c) {
    float xDist = abs(p.x - c.x);
    float yDist = abs(p.y - c.y);
    return (float) sqrt(pow(xDist, 2) + pow(yDist, 2));
}

int main(int argc, char *argv[]) {

    auto start = std::chrono::high_resolution_clock::now();

    string filename;
    int k = 3;

    if (argc > 0) {
        for (int i = 1; i < argc; i++) {
            string arg = argv[i];

            if (arg == "--help" || arg == "-h") {
                if (argc > 2) {
                    cerr << "Please only use the --help argument alone, without any other arguments";
                    exit(1);
                } else {
                    cout << "K-means algorithm implementation in C++" << endl;
                    cout << "Arguments: " << endl;
                    cout << '\t' << "-f [filename]" << '\t'
                         << "[Mandatory, no default value] Select an input file with point coordinates. The file must only contain the coordinates in this pattern \"X1 Y1,X2 Y2,X3 Y3\" ... for example 3 1,2.33 3,4 3,2.03 1,9.1 0"
                         << endl;
                    cout << '\t' << "Example usage: " << '\t' << "kmeans.exe -f source.txt" << endl;
                    cout << '\t' << "-k [integer]" << '\t'
                         << "Sets the number of clusters. Must be greater than 0, if not set, defaults to 3." << endl;
                    cout << '\t' << "Example usage: " << '\t' << "kmeans.exe -f source.txt -k 5" << endl;
                    cout << '\t' << "--help" << '\t' << "Prints this manual" << endl;
                    cout << '\t' << "Example usage: " << '\t' << "kmeans.exe --help" << endl;
                    exit(0);
                }
            }
            else if (arg == "-f") {
                if (++i >= argc) {
                    cerr << "after -f argument, filename must follow" << endl;
                } else {
                    filename = argv[i];
                }
            }
            else if (arg == "-k") {
                if (++i >= argc) {
                    cerr << "after -k argument, a number must follow" << endl;
                } else {
                    try {
                        k = stoi(argv[i]);
                        if (k < 1) {
                            cerr << "-k value must be greater than 0";
                            exit(1);
                        }
                    }
                    catch (exception ex) {
                        cerr << "after -k argument, an integer must follow";
                        exit(1);
                    }

                }
            }
            else {
                cerr << "Unknown argument: " << arg;
                exit(1);
            }
        }
    }
    ifstream inputStream(filename);
    if (inputStream.is_open()) {
        cout << "Input file opened successfully" << endl;
    } else {
        cerr << "Input file not found" << endl;
        exit(1);
    }

    std::string input((std::istreambuf_iterator<char>(inputStream)),
                      std::istreambuf_iterator<char>());

    inputStream.close();

    vector<point> pointList;
    vector<centroid> centroidList;

    try {
        vector<string> pointStrings = splitString(input, ",");
        for (auto element : pointStrings) {
            vector<string> pointCoordStrings = splitString(element, " ");
            point p(stof(pointCoordStrings[0]), stof(pointCoordStrings[1]));
            pointList.push_back(p);
        }
    }
    catch (std::exception &ex) {
        cerr << "Failed creating points from input file, please make sure data is in correct format. Use --help to see the allowed format.";
        exit(1);
    }

    float maxX = 0;
    float maxY = 0;
    for (auto p : pointList) {
        if (p.x > maxX) maxX = p.x;
        if (p.y > maxY) maxY = p.y;
        cout << "point: X: " << p.x << " Y: " << p.y << endl;
    }

    cout << "MaxX: " << maxX << endl;
    cout << "MaxY: " << maxY << endl;


    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distrX(0, maxX);
    std::uniform_int_distribution<> distrY(0, maxY);

    for (int i = 0; i < k; i++) {
        float randX = distrX(gen);
        float randY = distrY(gen);
        centroidList.push_back(centroid(randX, randY));
    }

    for (auto c : centroidList) {
        cout << "Centroid: X: " << c.x << " Y: " << c.y << endl;
    }

    while (!centroidsReachedEquilibrium(centroidList)) {
        clearPointsFromCentroids(centroidList);
        assignPointsToCentroids(pointList, centroidList);
        centerAllCentroids(centroidList);
    }
    for (auto c : centroidList) {
        c.print();
    }

    float maxValue = max(maxX, maxY);
    drawSvg(centroidList, maxValue);


    auto end = std::chrono::high_resolution_clock::now();
    cout << endl << "Process finished in " << to_ms(end - start).count() << " ms." << endl;
    exit(0);

}

void drawSvg(vector<centroid> &centroids, float max) {
    Dimensions dimensions(1000, 1000);
    //50 = padding;
    float multiplier = (1000 - 50) / max;
    Document doc("kmeans.svg", Layout(dimensions, Layout::BottomLeft));
    doc << Rectangle(Point(0, 0), 1000, 1000, Fill(Color::White));
    for (auto &centroid : centroids) {
        //offset by 50 to avoid very black points
        int r = 205 * centroid.x / max + 50;
        int g = 205 * centroid.y / max + 50;
        int b = 205 * (centroid.x + centroid.y) / (2 * max) + 50;
        Color currentColor(r, g, b);
        //25 offset to avoid points overlapping image edges
        doc << Rectangle(Point(25 + centroid.x * multiplier - 15, 25 + centroid.y * multiplier + 15), 30, 30,
                         Fill(currentColor), Stroke(2, Color::Black));
        for (auto &point : centroid.assignedPoints) {
            doc << Circle(Point(25 + point.x * multiplier, 25 + point.y * multiplier), 10, Fill(currentColor),
                          Stroke(2, Color::Black));
        }
    }
    doc.save();

}

void centerAllCentroids(vector<centroid> &centroids) {
    for (auto &centroid : centroids)
        centroid.center();
}

void assignPointsToCentroids(vector<point> points, vector<centroid> &centroids) {
    for (auto &point : points) {
        centroid *centroid = findNearestCentroid(point, centroids);
        centroid->assign(point);
    }

}

centroid *findNearestCentroid(point point, vector<centroid> &centroids) {

    float shortestDistance = INFINITY;
    centroid *nearestCentroid = nullptr;
    for (centroid &centroid : centroids) {
        auto distance = calculate_distance(point, centroid);
        if (distance < shortestDistance) {
            shortestDistance = distance;
            nearestCentroid = &centroid;
        }
    }
    return nearestCentroid;
}

void clearPointsFromCentroids(vector<centroid> &centroids) {
    for (auto &centroid : centroids)
        centroid.clearPoints();

}

bool centroidsReachedEquilibrium(vector<centroid> &centroids) {
    for (auto &centroid : centroids) {
        if (centroid.changedPosition) return false;
    }
    return true;
}