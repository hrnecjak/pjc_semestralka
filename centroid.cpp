//
// Created by Jakub on 12.01.2021.
//

#include <iostream>
#include <complex>
#include "centroid.h"

using namespace std;

centroid::centroid(float x, float y) : x(x), y(y) {}

bool cmpf(float A, float B, float epsilon = 0.005f) {
    return (fabs(A - B) < epsilon);
}

void centroid::center() {
    float oldX = this->x, oldY = this->y;
    float totalX = 0, totalY = 0;
    for (auto point : assignedPoints) {
        totalX += point.x;
        totalY += point.y;
    }
    if (!assignedPoints.empty()) {
        this->x = totalX / assignedPoints.size();
        this->y = totalY / assignedPoints.size();
    }
    this->changedPosition = !(cmpf(this->x, oldX) && cmpf(this->y, oldY));
}

void centroid::assign(point &point) {
    assignedPoints.push_back(point);
}

void centroid::clearPoints() {
    assignedPoints.clear();

}

void centroid::print() {
    cout << "\r\nCentroid at [" << this->x << ", " << this->y << "], assigned points ["<<assignedPoints.size()<<"] : " << endl;
    for (auto p: assignedPoints) {
        cout << "[" << p.x << ", " << p.y << "]" << endl;
    }
}



