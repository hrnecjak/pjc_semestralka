Semestrální práce Kmeans - Jakub Hrneček

Zadání:
K-means je jednoduchý iterativní algoritmus pro shlukování množiny bodů do k skupin (kde k je známé předem) s jednoduchou paralelizací. Vstupem je množina bodů (soubor) a počet skupin (parametr na příkazové řádce), na výstupu vrací seznam bodů a jejich příslušnost do skupiny. Výsledek je vhodné vizualizovat výstupem do obrázku SVG.

Implementace:
Moje řešení je jednovláknové. Přijímá soubor s množinou bodů (prostý text ve formátu "X1 Y1,X2 Y2,X3 Y3,..."), výstupem je 2D obrázek ve formátu SVG. Pro vykreslování do SVG jsem použil jednoduchou knihovnu SimpleSVG.
Program v průběhu vypisuje načtené body a náhodně vygenerované centroidy.
Po doběhnutí
Pro přehled funkcí je implementovaný přepínač --help.
Ukázkový vstupní soubor je v repozitáři pod názvem 'input.txt'
