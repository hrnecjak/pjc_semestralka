//
// Created by Jakub on 12.01.2021.
//

#ifndef KMEANS_CENTROID_H
#define KMEANS_CENTROID_H


#include <vector>
#include "point.h"

using namespace std;


class centroid {
public:
    std::vector<point> assignedPoints;
    float x,y;
    bool changedPosition = true;
    centroid(float x, float y);
    void center();
    void assign(point &point);
    void clearPoints();
    void print();

    void test();
};


#endif //KMEANS_CENTROID_H
